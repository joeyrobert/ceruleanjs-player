$(document).ready(function () {
    // Elements
    var ceruleanjsEl = $('#ceruleanjs');
    var terminalEl = ceruleanjsEl.find('#terminal');
    var terminalPreEl = ceruleanjsEl.find('#terminal pre');
    var commandEl = ceruleanjsEl.find('#command');
    var innerBoard = ceruleanjsEl.find('#inner');
    var outerBoard = ceruleanjsEl.find('#outer');
    var statusEl = ceruleanjsEl.find('#status');
    var fenEl = ceruleanjsEl.find('#fen');
    var pgnEl = ceruleanjsEl.find('#pgn');
    var goEl = ceruleanjsEl.find('#go');
    var flipEl = ceruleanjsEl.find('#flip');
    var undoEl = ceruleanjsEl.find('#undo');
    var twodEl = ceruleanjsEl.find('#2d');
    var threedEl = ceruleanjsEl.find('#3d');
    var newEl = ceruleanjsEl.find('#new');
    var downloadEl = ceruleanjsEl.find('#download');
    var thinkingEl = ceruleanjsEl.find('#thinking');
    var speedEl = ceruleanjsEl.find('#speed');
    var autoEl = ceruleanjsEl.find('#auto');
    var webglEl = ceruleanjsEl.find('#webgl-message');

    var buttonsToDisableWhileThinking = [
        newEl,
        goEl,
        undoEl,
        speedEl,
        autoEl
    ];

    var MOVE_REGEX = /^[a-h][1-8][a-h][1-8][bnrq]?$/;
    var GO_REGEX = /^go$/;
    var NEW_REGEX = /^new$/;
    var webGLEnabled = ChessBoard3.webGLEnabled();

    // Chess
    var worker = new Worker('/dist/ceruleanjs.js');
    var game;
    var board;
    var dimensions = webGLEnabled ? 3 : 2;
    var orientation = 'white';
    var thinking = false;
    var auto = false;
    var speeds = {
        fast: 1,
        normal: 5,
        slow: 15
    };

    function setupGame() {
        game = new Chess();
    }

    function appendLine(line) {
        var htmlLine = ansi_up.ansi_to_html(line.replace(/ /g, '&nbsp;'));
        terminalPreEl.append(htmlLine + '\n');
        terminalEl.scrollTop(terminalPreEl.height());
    }

    worker.onmessage = function (e) {
        var line = e.data;
        appendLine(line);

        if (line.indexOf('move ') === 0) {
            addMoveString(line.slice(5));

            if (auto) {
                setTimeout(function () {
                    sendCommand('go');
                }, 500);
            }
        }
    };

    function addMove(source, target, promotion) {
        var move = game.move({
            from: source,
            to: target,
            promotion: promotion || 'q'
        });

        if (move === null) {
            return 'snapback'
        }

        updateStatus();

        return true;
    }

    function addMoveString(move) {
        var from = move.slice(0, 2);
        var to = move.slice(2, 4);
        var promotion = move.slice(4, 5);
        var legalMove = addMove(from, to, promotion);
        board.position(game.fen(), true);

        if (legalMove !== 'snapback') {
            if (!auto) {
                setThinking(false);
            }
            return true;
        }
    }

    function onDragStart(source, piece, position, orientation) {
        if (game.game_over() === true ||
                (game.turn() === 'w' && piece.search(/^b/) !== -1) ||
                (game.turn() === 'b' && piece.search(/^w/) !== -1)) {
            return false;
        }
    }

    function onSnapEnd() {
        board.position(game.fen(), true);
    }

    function onDrop(source, target) {
        var lastRank = game.star

        var legalMove = addMove(source, target);

        if (legalMove !== 'snapback') {
            worker.postMessage(source + target);
        }

        return legalMove;
    }

    function updateStatus() {
        var status = '';

        var moveColor = 'White';
        if (game.turn() === 'b') {
            moveColor = 'Black';
        }

        if (game.in_checkmate() === true) {
            status = 'Game over, ' + moveColor + ' is in checkmate.';
        } else if (game.in_draw() === true) {
            status = 'Game over, drawn position';
        } else {
            status = moveColor + ' to move';

            if (game.in_check() === true) {
                status += ', ' + moveColor + ' is in check';
            }
        }

        statusEl.html(status);
        fenEl.html(game.fen());
        pgnEl.html(game.pgn());
    }

    function setupBoard() {
        var config = {
            backgroundColor: 0xffffff,
            whitePieceColor: 0xdddddd,
            position: game.fen(),
            orientation: orientation,
            draggable : true,
            showNotation: true,
            dropOffBoard: 'snapback',
            pieceTheme: '/vendor/chessboard/img/chesspieces/wikipedia/{piece}.png',
            pieceSet: '/vendor/chessboard3/assets/chesspieces/classic/{piece}.json',
            onDragStart: onDragStart,
            onDrop: onDrop,
            onSnapEnd: onSnapEnd,
        };

        if (board !== undefined) {
            board.destroy();
        }

        if (dimensions === 3) {
            innerBoard.css('width', '100%');
            board = new ChessBoard3('inner', config);
        } else {
            innerBoard.css('width', '75%');
            board = new ChessBoard('inner', config);
        }
    }

    function setThinking(thnkng) {
        thinking = thnkng;

        buttonsToDisableWhileThinking.map(function (el) {
            thinking ? el.attr('disabled', 'disabled') : el.removeAttr('disabled');
        });

        thinking ? thinkingEl.fadeIn(100) : thinkingEl.fadeOut(100);
    }

    function setSpeed(speedName) {
        sendCommand('st ' + speeds[speedName]);
    }

    function newGame() {
        setupGame();
        updateStatus();
        setupBoard();
        orientation = 'white';
        auto = false;
        board.orientation(orientation);
    }

    function sendCommand(line) {
        line = line.trim();

        if (MOVE_REGEX.test(line)) {
            addMoveString(line);
        }

        if (GO_REGEX.test(line) && board) {
            setThinking(true);
        }

        if (NEW_REGEX.test(line) && board) {
            newGame();
        }

        worker.postMessage(line);
        appendLine(line);
        commandEl.val('');
    }

    setupGame();
    updateStatus();
    setupBoard();
    commandEl.focus();
    sendCommand('help');
    setSpeed('normal');
    setThinking(false);

    commandEl.on('keypress', function (e) {
        if (e.which === 13) {
            var line = commandEl.val();
            sendCommand(line);
            return false;
        }
    });

    twodEl.click(function () {
        dimensions = 2;
        setupBoard();
    });

    if (webGLEnabled) {
        threedEl.click(function () {
            dimensions = 3;
            setupBoard();
        });
    } else {
        webglEl.show();
        twodEl.hide();
        threedEl.hide();
    }

    goEl.click(function () {
        sendCommand('go');
    });

    flipEl.click(function () {
        orientation = orientation === 'white' ? 'black' : 'white';
        board.orientation(orientation);
    });

    undoEl.click(function () {
        sendCommand('remove');
        game.undo();
        game.undo();
        board.position(game.fen(), true);
    });

    newEl.click(function () {
        sendCommand('new');
        newGame();
    });

    newEl.click(function () {
        sendCommand('new');
        newGame();
    });

    autoEl.click(function () {
        auto = true;
        sendCommand('go');
    });

    speedEl.change(function () {
        setSpeed(speedEl.val());
    });
});